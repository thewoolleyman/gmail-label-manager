require 'googleauth'
require 'googleauth/stores/file_token_store'

# https://github.com/googleapis/google-auth-library-ruby#example-command-line
class CliAuth
  OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'

  def self.get_creds
    oob_uri = 'urn:ietf:wg:oauth:2.0:oob'

    scope = 'https://www.googleapis.com/auth/gmail.labels'
    # e.g. 'client_secret_677774294340-dg878ec10tnt6b6g5do7268ef577jklg.apps.googleusercontent.com.json'
    client_secret_file = ENV.fetch('CLIENT_SECRET_FILE')
    client_id = Google::Auth::ClientId.from_file(client_secret_file)
    token_store = Google::Auth::Stores::FileTokenStore.new(
      file: "#{ENV['HOME']}/.gmail-label-manager-tokens.yaml"
    )
    authorizer = Google::Auth::UserAuthorizer.new(client_id, scope, token_store)

    # e.g. thewoolleyman
    user_id = ENV.fetch('GMAIL_USER_ID')
    creds = authorizer.get_credentials(user_id)
    if creds.nil?
      url = authorizer.get_authorization_url(base_url: oob_uri)
      puts "Open #{url} in your browser and enter the resulting code:"
      code = gets
      creds = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: oob_uri)
    end
    creds
  end
end

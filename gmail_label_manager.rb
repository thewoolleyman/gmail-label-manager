#!/usr/bin/env ruby

require 'google/apis/gmail_v1'
require_relative 'cli_auth'

class GLM
  def run
    creds = CliAuth.get_creds
    gmail = Google::Apis::GmailV1::GmailService.new
    gmail.authorization = creds
    all_labels = gmail.list_user_labels('me').labels
    label = all_labels.detect { |label| label.name = 'test-label' }
    p label
    p label.methods.sort - 1.methods
    p label.name
    p label.id
    p label.name = ".test-#{label.name}"
    p gmail.update_user_label('me', label.id, label)
  end
end

GLM.new.run